import React, {Component} from 'react';
import { Container } from 'reactstrap';


import '../css/BodyIndex.css';

class BodyIndex extends Component {
  render(){
    return (
      <div className="bodyIndex">
        <Container>
          <div className="bodyIndexContent">
            Athena
          </div>
        </Container>
      </div>
    );
  }
}
export default BodyIndex;
